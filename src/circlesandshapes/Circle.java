/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package circlesandshapes;

/**
 *
 * @author mymac
 */
public class Circle extends Shape {
    private double radius;
    
    Circle(){
    }
    
    Circle(double radius){
       
        this.radius = radius;
    }
    
    public double getArea(){
        return Math.PI * Math.sqrt(radius);
    }
    
    public double getPerimeter(){
        return (radius*2) * Math.PI;
        
    }
    
     public String toString(){
     return "The area of the circle is: " + getArea() + 
     "\nThe perimeter of the circle is: " + getPerimeter();
    }
}
