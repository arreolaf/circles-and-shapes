/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package circlesandshapes;

/**
 *
 * @author mymac
 */
public class Square extends Shape {
    private double sideLength;
    
    Square(){
        
    }
    
    Square(double sideLength){
      
        this.sideLength = sideLength;
    }
    
    public double getArea(){
        return Math.pow(sideLength, 2);
    }
    
    public double getPerimeter(){
        return sideLength*4;
    }
    
    public String toString(){
        return "The area of the square is: " + getArea() + 
        "\nThe perimeter of the square is: " + getPerimeter();
    }
}
