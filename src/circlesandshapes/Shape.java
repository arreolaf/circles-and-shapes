/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package circlesandshapes;

/**
 *
 * @author mymac
 */
public class Shape {
    private double area;
    private double perimeter;
    
    Shape(){
        
    }
    
    Shape(double area, double perimeter){
        this.area = area;
        this.perimeter = perimeter;
    }
    
    public double getArea(){
        return this.area;
    }
    
    public double getPerimeter(){
        return this.perimeter;
    }
}
